import datetime
import pytz
import json
from pprint import pprint

from birdy.twitter import UserClient

config = {}
execfile("config.py", config)
client = UserClient(config["consumer_key"],
                    config["consumer_secret"],
                    config["access_key"],
                    config["access_secret"])

resource = client.api.users.show
response = resource.get(screen_name='twitter')
processed_count = 0
fetch_more_tweets = True
min_tweet_id = None
is_initial_fetch = True
term = "http://www.infobae.com/politica/2017/12/05/the-economist-y-un-duro-articulo-sobre-la-falta-de-inversion-militar-en-la-argentina-despilfarro-y-corrupcion/"
response = client.api.search.tweets.get(q=term, count=100, max_id=None)
print "term[%s] _> " % (term)
last_tweet_id = response.data.search_metadata.max_id_str if is_initial_fetch else None
print "Last tweet id: (%s) " % (last_tweet_id)
tweets = response.data.statuses
if tweets:
    processed_count += len(tweets)
    print "Processed count: [%s] " % (processed_count)
    min_tweet_id = str(min([t.id for t in tweets]) - 1)
    print "Min tweet id: [%s] " % (min_tweet_id)


fetch_more_tweets = len(tweets) == 100 and u'next_results' in response.data.search_metadata and (limit is None or processed_count < limit)
is_initial_fetch = False    
print "Fecth more tweets: [%s] " % (fetch_more_tweets)

response = client.api.statuses.lookup.get(id=min_tweet_id)
data = {
        'favorites_count': sum([t.favorite_count for t in response.data]),
        'retweets_count': sum([t.retweet_count for t in response.data])
        }
print "Data: [%s] " % (data)
print "__________TWEET (%s) INFO_____________" % (last_tweet_id)
tweet_info = client.api.statuses.show[last_tweet_id].get()
count = tweet_info.data.favorite_count
print "__________TWEET (%s) COUNT FAVORITE( %s )_____________" % (last_tweet_id, count)

#print json.dumps(tweet_info.data,  sort_keys=True, indent=4, separators=(',', ': '))
#print json.dumps(tweet_info.data.user,  sort_keys=True, indent=4, separators=(',', ': '))
print json.dumps(tweet_info.data.user.screen_name,  sort_keys=True, indent=4, separators=(',', ': '))

def update_mentions_using_provider(client, screen_name):
    #client = provider.client()
    is_initial_fetch = True
    processed_count = 0
    fetch_more_tweets = True
    query = "to:%s" % (screen_name)
    begin_time = datetime.datetime.now(pytz.utc)
    
    last_fetch_time = begin_time - datetime.timedelta(hours=4)
    print last_fetch_time
    last_fetch_time = begin_time - datetime.timedelta(days=1)
    print last_fetch_time.strftime('%Y-%m-%d')
    min_tweet_id = None
    #query = query + ' result_type:{}'.format("mixed")
    query = query + ' since:{}'.format(last_fetch_time.strftime('%Y-%m-%d')) if last_fetch_time is not None else query
    while fetch_more_tweets:
        try:
            if is_initial_fetch:
                response = client.api.search.tweets.get(q=query, count=100)
            else:
                response = client.api.search.tweets.get(q=query, count=100, max_id=min_tweet_id)
        except TypeError:
            print 'Rate limit reached for {} - Swicthing client.'.format(provider.current_credential.access_token)
            #logger.info('Rate limit reached for {} - Swicthing client.'.format(provider.current_credential.access_token))
            #client = provider.new_client()
        else:
            mentions = response.data.statuses
            processed_count += len(mentions)
            fetch_more_tweets = (len(mentions) == 100
                                 and u'next_results' in response.data.search_metadata
                                 and (limit is None or processed_count < limit))
            is_initial_fetch = False
            datai = []
            if mentions:
                min_tweet_id = str(min([t.id for t in response.data.statuses]) - 1)
                datai = {t.id: t for t in response.data.statuses}
                print "Data: [%s] " % (min_tweet_id)
                print "Longitud data: [%s] " % (len(datai))
                #print json.dumps(datai,  sort_keys=True, indent=4, separators=(',', ': '))
                for tweet_id, m in datai.iteritems():
                    print "tweet_id[%s] - in_reply_to_status_id_str[%s]" % (tweet_id, m.in_reply_to_status_id_str)
                replies = [m for tweet_id, m in datai.iteritems() if m.in_reply_to_status_id_str == '940712932207448069']
                print json.dumps(replies,  sort_keys=True, indent=4, separators=(',', ': '))
            return datai
                #rd_client.hmset(screen_name, {t.id: json.dumps(t) for t in response.data.statuses})

data = update_mentions_using_provider(client, 'titulame')
def fetch_replies(data, tweet_idx='940712932207448069'):
    mentions = data
    print tweet_idx
    #for tweet_idx, m in mentions.iteritems():
        #print tweet_idx
        #print json.dumps(m,  sort_keys=True, indent=4, separators=(',', ': '))
    replies = [m for tweet_id, m in mentions.iteritems() if m.in_reply_to_status_id_str == tweet_idx]
    return replies

if data:
    #q=fetch_replies(data)
    print "--------------------------"
    #print q
    #print json.dumps(q,  sort_keys=True, indent=4, separators=(',', ': '))
# CACHES = { 
#     'default': { 
#         'BACKEND': 'django.core.cache.backends.dummy.DummyCache', 
#         # 'BACKEND': 'django.core.cache.backends.db.DatabaseCache', 
#         # 'LOCATION': 'news_cache_table', 
#         'BACKEND': 'django.core.cache.backends.db.DatabaseCache', 
#         'LOCATION': 'news_cache_table', 
#     } 
# } 

