#!/usr/bin/python

#-----------------------------------------------------------------------
# twitter-search
#  - performs a basic keyword search for tweets containing the keywords
#    "lazy" and "dog"
#-----------------------------------------------------------------------
import datetime
import pytz
import json
import twitter

#-----------------------------------------------------------------------
# load our API credentials 
#-----------------------------------------------------------------------
config = {}
execfile("config.py", config)

def oauth_login(config):
	# XXX: Go to http://twitter.com/apps/new to create an app and get values
	# for these credentials that you'll need to provide in place of these
	# empty string values that are defined as placeholders.
	# See https://dev.twitter.com/docs/auth/oauth for more information
	# on Twitter's OAuth implementation.
	CONSUMER_KEY = ''
	CONSUMER_SECRET = ''
	OAUTH_TOKEN = ''
	OAUTH_TOKEN_SECRET = ''
	auth = twitter.oauth.OAuth(config["access_key"], config["access_secret"],	config["consumer_key"], config["consumer_secret"])
	twitter_api = twitter.Twitter(auth=auth)
	return twitter_api

def twitter_search(twitter_api, q, max_results=200, **kw):
	search_results = twitter_api.search.tweets(q=q, count=100, **kw)
	statuses = search_results['statuses']
	max_results = min(1000, max_results)
	for _ in range(10): # 10*100 = 1000
		try:
			next_results = search_results['search_metadata']['next_results']
		except KeyError, e: # No more results when next_results doesn't exist
			break
		kwargs = dict([ kv.split('=') for kv in next_results[1:].split("&") ])
		search_results = twitter_api.search.tweets(**kwargs)
		statuses += search_results['statuses']
		if len(statuses) > max_results:
			break
	return statuses

# Sample usage
twitter_api = oauth_login(config)

print twitter_api

q = "https://www.cronica.com.ar/politica/Detienen-a-manifestantes-con-palos-piedras-y-gomeras-20171218-0022.html"
results = twitter_search(twitter_api, q, max_results=10)
# Show one sample search result by slicing the list...
#print json.dumps(results[0], indent=1))
print json.dumps(results[0],  sort_keys=True, indent=4, separators=(',', ': '))